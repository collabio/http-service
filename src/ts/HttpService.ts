import express = require('express');
import bodyParser = require('body-parser');
import cookieParser = require('cookie-parser');
import { MongoClient, Binary, Collection } from 'mongodb';
import { v4 as UuidV4 } from 'node-uuid';
import * as fs from 'fs';
import * as path from 'path';

const __server = path.resolve(require.main.filename, '..', '..', '..');

function auth(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    let method = target[propertyKey];
    target[propertyKey] = async function (req, res) {
        if (typeof req.cookies.authToken === 'string') {
            let bytes = Buffer.from(req.cookies.authToken, 'base64');
            let authToken = new Binary(bytes, Binary.SUBTYPE_UUID);
            let userId = bytes.readUInt32BE(0);

            let user = await this.users.findOne({ _id: userId, authToken });

            if (user)
                return method.call(this, req, res, user);
        }
        res.sendStatus(401);
    };
}

function setRoute(method: string, route: string) {
    return function (target: HttpService, propertyKey: string, descriptor: PropertyDescriptor) {
        HttpService.api.push({
            method, route, getFunc: () => { return target[propertyKey]; }
        });
    };
}

function get(route: string) {
    return setRoute('get', route);
}

function post(route: string) {
    return setRoute('post', route);
}

function put(route: string) {
    return setRoute('put', route);
}

function patch(route: string) {
    return setRoute('patch', route);
}

function del(route: string) {
    return setRoute('delete', route);
}

export class HttpService {

    private users: Collection;
    private autoincrement: Collection;
    private projects: Collection;
    public static api = [];

    public constructor(protected app: express.Express) {
        this.initialize();
    }

    @auth
    @get('/users/:id')
    public getUser(req, res, user) {
        if (req.params.id == user._id)
            res.send(user);
        else
            res.sendStatus(401);
    }

    @post('/login')
    public async login(req, res) {
        let user = await this.users.findOne({ login: req.body.login, password: req.body.password });

        let uuid = UuidV4(null, new Buffer(20), 4);
        uuid.writeUInt32BE(user._id, 0);
        let authToken = new Binary(uuid, Binary.SUBTYPE_UUID);
        let query = await this.users.findOneAndUpdate({ _id: user._id }, { $set: { authToken } }, { returnOriginal: false });

        res.send(query.ok ? query.value : { error: 'Bad login info.' });
    }

    @put('/users')
    public async register(req, res) {
        let user = req.body;
        user._id = (await this.autoincrement.findOneAndUpdate(
            { _id: 'users' }, { $inc: { sequence: 1 } }, { returnOriginal: false }
        )).value.sequence;

        await this.users.insertOne(user);

        res.send({});
    }

    @auth
    @get('/projects')
    public async getProjects(req, res, user) {
        res.send(await this.projects.find({ members: user.login }, { _id: 1, name: 1, members: 1 }).toArray());
    }

    @auth
    @get('/projects/:id')
    public async getProjectById(req, res, user) {
        res.send(await this.projects.findOne({ _id: parseInt(req.params.id) }));
    }

    @auth
    @put('/projects')
    public async newProject(req, res, user) {
        let projectId = (await this.autoincrement.findOneAndUpdate(
            { _id: 'projects' }, { $inc: { sequence: 1 } }, { returnOriginal: false }
        )).value.sequence;

        let project = req.body;
        project._id = projectId;
        project.members = [user.login];

        this.projects.insertOne(project);
        res.send({});
    }

    @auth
    @patch('/projects/:id')
    public async updateProject(req, res, user) {
        let project = req.body;
        if (project._id == req.params.id) {
            await this.projects.save(project);
            res.send({});
        } else {
            res.send({ error: 'Error occured while trying to update.' });
        }
    }

    @auth
    @del('/projects/:id')
    public async deleteProject(req, res, user) {
        
        await this.projects.remove({ _id: parseInt(req.params.id) });
        res.send({});
    }

    @auth
    @put('/projects/:id/scenes')
    public async newScene(req, res, user) {
        let scene = req.body;
        scene._id = (await this.autoincrement.findOneAndUpdate(
            { _id: 'scenes' }, { $inc: { sequence: 1 } }, { returnOriginal: false }
        )).value.sequence;

        let project = (await this.projects.findOneAndUpdate({ _id: parseInt(req.params.id) }, { $push: { scenes: scene } }, { returnOriginal: false })).value;
        res.send(project.scenes.find(s => s._id = scene._id));
    }

    @auth
    @get('/projects/:id/assets')
    public async getProjectAssets(req, res, user) {

        let project = await this.projects.findOne({ _id: parseInt(req.params.id) }, { fields: { assets: 1 } });

        res.send(project.assets);
    }

    @auth
    @get('/projects/:projectId/scenes/:sceneId')
    public async getSceneById(req, res, user) {

        let sceneId = parseInt(req.params.sceneId);
        let project = await this.projects.findOne({ _id: parseInt(req.params.projectId), 'scenes._id': sceneId }, { fields: { scenes: 1 } });

        res.send((project && project.scenes.find(scene => scene._id === sceneId)) || {});
    }

    @auth
    @put('/projects/:id/assets')
    public async newAsset(req, res, user) {
        let assetData = req.body;
        let uuid = UuidV4();
        let filepath = `/assets/${uuid.toString()}.${assetData.extension}`;
        let asset = {
            _id: uuid,
            name: assetData.name,
            url: filepath
        }

        fs.writeFileSync(path.join(__server, filepath), assetData.data, { encoding: 'base64' });
        let project = (await this.projects.findOneAndUpdate({ _id: parseInt(req.params.id) }, { $push: { assets: asset } }, { returnOriginal: false })).value;

        res.send(project.assets.find(a => a._id == uuid));
    }

    private async initialize() {
        let db = await MongoClient.connect('mongodb://localhost:27017/collabio');

        this.autoincrement = db.collection('autoincrement');
        this.users = db.collection('users');
        this.projects = db.collection('projects');

        if (!await this.autoincrement.findOne({ _id: 'users' }))
            await this.autoincrement.insert({ _id: 'users', sequence: 0 });
        if (!await this.autoincrement.findOne({ _id: 'projects' }))
            await this.autoincrement.insert({ _id: 'projects', sequence: 0 });
        if (!await this.autoincrement.findOne({ _id: 'scenes' }))
            await this.autoincrement.insert({ _id: 'scenes', sequence: 0 });

        let router = express.Router();

        router.use(bodyParser.json({ limit: '100mb' }));
        router.use(cookieParser());

        for (let route of HttpService.api) {
            router[route.method](route.route, route.getFunc().bind(this));
        }

        this.app.use('/api', router);
    }
}